<?php
    $main_elevator = new Elevator();
    $debug_error;
    $report;
    $main_elevator->floor = (int)$_POST['origin'];
    $main_elevator->maintenance_floors = explode(',',$_POST['maintenance']);
    $floors = explode(',',$_POST['floors']);
    $filtered_floors = [];
    

    if($main_elevator->validateData($floors, $main_elevator->mainetanceFloors)){
        $filtered_floors = deleteMaintenanceFloors($floors, $main_elevator->maintenance_floors);
        $report = $main_elevator->maintenanceFloors($main_elevator->maintenance_floors) . "<br>" . $main_elevator->moveElevator($filtered_floors, " ");
    } else {
        $debug_error = "Ingrese solo valores numericos <br>";
    }

    echo "<div class='container center-align'>";
    echo "<div class='card-panel'>";
    echo "<h4 class='center-align'>Reporte</h4>";
    echo $debug_error;
    echo $report;
    echo "</div>";
    echo "</div>";
?>