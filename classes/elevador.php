<?php
    class Elevator
    {
        public $state = "detenido";
        public $floor = 0;
        public $target_floor;
        public $maintenance_floors = ["3","7"];
        public function directionElevator($target)
        {
            if($target > $this->floor){
                $this->state = "sube";
            } else if ($target < $this->floor){
                $this->state = "baja";
            } else{
                $this->state = "detenido";
            }
            return $this->state;
        }

        public function moveElevator($targets, $result)
        {
            $piso_aux;
            do {
                $this->target_floor = current($targets);
                if($this->verifyFloor($this->floor, $targets)){
                    $targets = array_diff($targets, [$this->floor]);
                    $result = $result . "Solicitud al piso " . (string)$this->floor . " completada" . "<br>";
                    $piso_aux = $this->floor;
                } else {
                    $result = $result . "Mover elevador de " . (string)$this->floor . " hacia " . (string)$this->target_floor . " " . $this->directionElevator($this->target_floor) . "<br>";
                    if($this->state == "sube"){
                        $this->floor = $this->floor + 1;
                    } else if ($this->state == "baja"){
                        $this->floor = $this->floor - 1;
                    }
                }
            } while (!count($targets) == 0 || !$targets == null);
            
            $this->state = "detenido";
            $result = $result . "Solicitudes completadas " . $this->state . " en el piso " . $piso_aux;
            return $result;
            
        }

        public function verifyFloor($floor, $targets)
        {
            if(in_array($floor, $targets)){
                return true;
            } 
            else {
                return false;
            }
        }

        public function maintenanceFloors($maintenance){
            $floors = "";
            foreach ($maintenance as $key => $floor) {
                $floors = $floors . (string)$floor . " ";
            }
            return "Pisos en mantenimiento " . $floors;
        }

        public function validateData($targets, $maintenance_floors){
            $valid = true;
            foreach ($targets as $key => $target) {
                if(!is_numeric($target)){
                    $valid = false;
                }
            }
            return $valid;
        }
    }

    function deleteMaintenanceFloors($targets, $maintenance_floors){
        foreach ($targets as $key => $target) {
            if(in_array($target, $maintenance_floors)){
                $targets = array_diff($targets, [$target]);
            }
        }
        return $targets;
    }

?>


