<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="custom.css">
    <title>Elevador</title>
</head>
<body class="blue darken-2">
    <div class="container">
        <div class="row card-panel">
            <h1 class="center-align">Simulador de elevador</h1>
            <form class="container" method="POST">
                <div class="input-field">
                    <input placeholder="Piso de origen" id="origin" class="validate" type="text" name="origin"/>
                </div>
                <div class="input-field">
                    <input placeholder="Pisos en mantenimiento" id="origin" class="validate" type="text" name="maintenance"/>
                </div>
                    
                <div class="input-field">
                    <input placeholder="Pisos destino" class="validate" type="text" name="floors"/>
                </div>
                    
                <div class="center-align">
                    <input class="orange accent-3 btn-large" type="submit" name="submit" value="MOVER ELEVADOR" />
                </div> 
            </form>
        </div>
        
    </div>
        
    <?php
        include('classes/elevador.php');
        include('logic/main.php')
    ?>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
</html>


